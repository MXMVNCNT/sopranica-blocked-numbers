# sopranica-blocked-numbers

## Installation
1. Clone this repository
```git clone https://codeberg.org/MXMVNCNT/hostile-to-jmp.git```
You need the `pandas` pip package. Go in your project's directory then type in a terminal:
```
cd hostile-to-jmp
pip install pandas
python3 csv-to-json/main.py
```
Thats all that is needed! Now open the index.html file in a browser to see the page. For a web server, simply point to this file. 

For the auto-refresh feature, I use a Cron Job every 24 hours as I think this polling is reasonnable (and is the one I agreed to with the site owner) to have accurate data and to not spam the original wiki page with requests!
Here is my Cron Job (you need to put the right path to .../csv-to-json/main.py)
```
# refresh the data for Hostile-to-jmp 
0 0 * * * python3 ~/hostile-to-jmp/csv-to-json/main.py
```

A UI for the wiki page of sopranica about the blocked phone numbers 

https://wiki.soprani.ca/FAQ/Why%20isn%27t%20my%20number%20being%20accepted%20by%20an%20online%20service%3F