"""
    JMP Numbers That Are Blocked or Not (CSV to JSON)
    Copyright (C) 2022  Maxime V.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from datetime import datetime
from threading import Thread
import requests
import pandas
import os


def readFile(filename):
    filehandle = open(filename)
    print(filehandle.read())
    filehandle.close()

fullDirectory = os.path.dirname(__file__)

def getData():
    url = "https://wiki.soprani.ca/FAQ/Why%20isn%27t%20my%20number%20being%20accepted%20by%20an%20online%20service%3F?action=raw"
    response = requests.get(url)
    return response.text

def generateCsv():
    csv_start = '#!CSV ,'
    csv_end = '}}}'
    lines = []
    lineToSkip = []
    contentStartLine = 0

    open(f"{fullDirectory}/input.txt", "w", encoding='utf-8').write(getData())

    input_file = open(f"{fullDirectory}/input.txt", "r")

    output_file = open(fullDirectory + r"/out.csv", 'w')

    lines = input_file.readlines()

    currentLine = 1
    currentLine2 = 1
    for line in lines:
        if line.startswith(csv_start):
            contentStartLine = currentLine
            lineToSkip.extend(range(currentLine + 1))
            lineToSkip.append(len(lines))
            break
        currentLine += 1

    lines[contentStartLine] = "Service,SignUpSuccess,2faSuccess,LastSuccess,SignUpFailure,2faFailure,LastFailure,AdditionalComments\n"

    for line in lines:
        if currentLine2 in lineToSkip:
            print(f"skipped line {currentLine2}")
            pass
        else:
            output_file.write(line)
            print(f"wrote line {currentLine2}")
        currentLine2 += 1

    input_file.close()
    output_file.close()

def csvToJson():
    df = pandas.read_csv (fullDirectory + r'/out.csv')
    df.to_json (fullDirectory + r'/../output.json', orient="records")
    last_edited = open(fullDirectory + r'/../last-edited', 'w').write(str(datetime.now().astimezone()))


t1 = Thread(target=generateCsv())
t2 = Thread(target=csvToJson())
t1.start()
t2.start()
t1.join()
t2.join()
